# Ce TP introduit les notions de parcours en profondeur et parcours en largeur

from TP1 import Graphe

class GrapheParcours (Graphe):
    # On travaille uniquement sur un graphe non-orienté

    def parcours_largeur(self, sommet_depart):
        # La file des prochains sommets à parcourir
        # Une file est de type FIFO : premier entré, premier sorti
        file = [sommet_depart]

        # L'arbre de parcours représente les arêtes entre les sommets
        # parcourus. Il respecte les distances découvertes au fur et à
        # mesure
        graphe_parcours = Graphe()

        # Les distances de chaque sommet par rapport au sommet d'origine
        # Chaque clé correspond au sommet, et à chaque clé est associée un chiffre
        # représentant la distance
        distances = dict()

        # Évidemment, le sommet de départ a une distance de 0 par rapport à lui-même
        distances[sommet_depart] = 0

        # Début du parcours
        while len(file) > 0:
            # On parcourt tant qu'il reste des sommets à parcourir,
            # autrement dit quand il y a toujours des éléments dans la file
            # donc sa taille est plus grande que 0

            sommet_actuel = file.pop(0)     # On enlève le premier élément (FIFO)

            # On récupère tous les voisins du sommet actuel
            for voisin in self._graphe_dict[sommet_actuel]:
                # On part du principe que si une distance a déjà été calculée
                # pour le voisin actuel, alors il a déjà été parcouru. On ne
                # va voir que les autres
                if voisin not in distances:
                    # On calcule la distance pour ce sommet voisin
                    distances[voisin] = distances[sommet_actuel] + 1
                    # On l'ajoute aux prochains sommets à parcourir (LIFO)
                    file.append(voisin)
                    # On ajoute l'arête dans le graphe
                    graphe_parcours.add_arete({sommet_actuel, voisin})

        return (distances, graphe_parcours)

    def parcours_profondeur(self, sommet_depart):
        # Même concept que le parcours en largeur avec l'utilisation
        # d'une pile (structure LIFO : dernier entré premier sorti)
        
        # La pile des prochains sommets à parcourir
        pile = [sommet_depart]
        
        graphe_parcours = Graphe()
        distances = dict()

        distances[sommet_depart] = 0

        # Début du parcours
        while len(pile) > 0:
            sommet_actuel = pile.pop()     # On enlève le premier élément (LIFO)
            for voisin in self._graphe_dict[sommet_actuel]:
                if voisin not in distances:
                    distances[voisin] = distances[sommet_actuel] + 1
                    pile.append(voisin)
                    graphe_parcours.add_arete({sommet_actuel, voisin})

        return (distances, graphe_parcours)


class GrapheAlgo (GrapheParcours):
    # Cette classe implémente plusieurs algorithmes utilisant le parcours en 
    # profondeur
    # Implémente les algorithmes de parcours à différentes fins
    def est_connexe(self, sommet_depart):
        # Utilise le parcours en profondeur pour renvoyer vrai si le graphe
        # est connexe et faux sinon
        file = [sommet_depart]
        # On garde les sommets parcourus en mémoire
        parcourus = [sommet_depart]

        while len(file) > 0:
            # On récupère le sommet à parcourir
            sommet_actuel = file.pop(0)
            # On récupère tous les voisins
            for sommet_voisin in self._graphe_dict[sommet_actuel]:
                if sommet_voisin not in parcourus:
                    parcourus.append(sommet_voisin)
                    file.append(sommet_voisin)

        # On compte le nombre de sommets parcourus et on le compare
        # au nombre de sommets du graphe
        return len(self.all_sommets()) == len(parcourus)


    def chemin_existe(self, sommet_depart, sommet_arrive):
        # Utilise le parcours en profondeur pour vérifier si un chemin
        # existe
        file = [sommet_depart]
        resultat = sommet_depart == sommet_arrive

        # On garde les sommets parcourus en mémoire
        parcourus = [sommet_depart]

        while len(file) > 0 and not resultat:
            # On récupère le sommet à parcourir
            sommet_actuel = file.pop(0)
            for sommet_voisin in self._graphe_dict[sommet_actuel]:
                # Si le sommet voisin 
                if sommet_voisin == sommet_arrive:
                    resultat = True

                if sommet_voisin not in parcourus:
                    parcourus.append(sommet_voisin)
                    file.append(sommet_voisin)

        return resultat


    def cycle_existe(self, sommet_depart):
        # Utilise le parcours en profondeur pour vérifier si un cycle existe
        file = [sommet_depart]
        resultat = False

        # On garde les sommets parcourus en mémoire
        parcourus = [sommet_depart]

        while len(file) > 0 and not resultat:
            # On récupère le sommet à parcourir
            sommet_actuel = file.pop(0)
            for sommet_voisin in self._graphe_dict[sommet_actuel]:
                # Si le sommet est déjà parcouru et qu'il fait partie des
                # voisins, alors on a bien un cycle
                if sommet_voisin in parcourus:
                    resultat = True
                else:
                    parcourus.append(sommet_voisin)
                    file.append(sommet_voisin)

        return resultat


    def chemin_largeur(self, sommet_depart, sommet_arrivee):
        # Utilise le parcours en profondeur pour trouver un chemin entre
        # le départ et l'arrivée
        file = [sommet_depart]

        # On garde un dictionnaire de chemins pour aller du sommet de départ
        # à n'importe quel sommet
        chemins = dict()
        chemins[sommet_depart] = (sommet_depart,)

        while len(file) > 0:
            # On récupère le sommet actuel
            sommet_actuel = file.pop(0)
            for sommet_voisin in self._graphe_dict[sommet_actuel]:
                # Si aucun chemin n'a été créé pour ce sommet voisin, il n'a pas été
                # parcouru
                if sommet_voisin not in chemins:
                    # On ajoute le chemin pour aller jusqu'a ce voisin et prenant
                    # le chemin du sommet actuel et en ajoutant le voisin
                    chemins[sommet_voisin] = chemins[sommet_actuel] + (sommet_voisin, )
                    file.append(sommet_voisin)

                    # On retourne ce chemin si ce voisin est le sommet d'arrivée
                    if sommet_voisin == sommet_arrivee:
                        return chemins[sommet_voisin]

        # Si on sort de la boucle, c'est qu'aucun chemin n'a été trouvé
        return tuple()


if __name__ == "__main__":
    graphe_data = {
        "A" : {"C"},
        "B" : {"C", "E"},
        "C" : {"A", "B", "D", "E"},
        "D" : {"C"},
        "E" : {"C", "B"},
        "F" : set()
    }

    graphe_data2 = {
        "A" : {"D", "F"},
        "B" : {"C"},
        "C" : {"B", "C", "D", "E"},
        "D" : {"A", "C", "F"},
        "E" : {"C"},
        "F" : {"A", "D"}
    }

    graphe_data3 = {
        "1" : {"2", "5"},
        "2" : {"1", "3", "5", "7"},
        "3" : {"2", "4", "5"},
        "4" : {"3", "6"},
        "5" : {"1", "2", "3", "6"},
        "6" : {"5", "7"},
        "7" : {"2", "6"}
    }

    g1 = GrapheAlgo(graphe_data)
    g2 = GrapheAlgo(graphe_data2)
    g3 = GrapheAlgo(graphe_data3)

    # On récupère séparement pour chaque parcours et pour chaque
    # graphe le dictionnaire des distances et le graphe de parcours
    # généré
    d1_largeur, t1_largeur = g1.parcours_largeur("A")
    d2_largeur, t2_largeur = g2.parcours_largeur("A")
    d3_largeur, t3_largeur = g3.parcours_largeur("1")

    d1_profondeur, t1_profondeur = g1.parcours_profondeur("A")
    d2_profondeur, t2_profondeur = g2.parcours_profondeur("A")
    d3_profondeur, t3_profondeur = g3.parcours_profondeur("1")

    # Affichage
    print("Parcours Largeur\n---------")
    print(d1_largeur, "\n", t1_largeur)
    print(d2_largeur, "\n", t2_largeur)
    print(d3_largeur, "\n", t3_largeur)

    print("\nParcours Profondeur\n---------")
    print(d1_profondeur, "\n", t1_profondeur)
    print(d2_profondeur, "\n", t2_profondeur)
    print(d3_profondeur, "\n", t3_profondeur)

    # Vérification de la connexité
    print("g1 connexe : ", g1.est_connexe("A"))
    print("g2 connexe : ", g2.est_connexe("A"))
    print("g3 connexe : ", g3.est_connexe("1"))

    # Vérification d'existence d'un cycle
    print("g1 contient un cycle : ", g1.cycle_existe("A"))
    print("g2 contient un cycle : ", g2.cycle_existe("A"))
    print("g3 contient un cycle : ", g3.cycle_existe("1"))

    # Vérification d'existence d'un chemin
    print("g1 chemin de A vers F : ", g1.chemin_existe("A", "F"), g1.chemin_largeur("A", "F"))
    print("g2 chemin de A vers B : ", g2.chemin_existe("A", "B"), g2.chemin_largeur("A", "B"))
    print("g3 chemin de 1 vers 4 : ", g3.chemin_existe("1", "4"), g3.chemin_largeur("1", "4"))