class Graphe(object):

    def __init__(self, graphe_dict=None):
        """
            Initialise un graphe. Sans dictionnaire passé,
            on utilise un dictionnaire vide
        """

        # Si le dictionnaire n'est pas donné, on en crée un
        if graphe_dict == None:
            graphe_dict = {}

        # Le dictionnaire aura pour clés les sommets et pour valeur
        # la liste des sommets voisins
        self._graphe_dict = graphe_dict

    def aretes(self, sommet):
        """
            Retourne la liste de toutes les arrêtes d'un sommet
        """
        # On vérifie d'abord que le sommet existe bien
        if sommet not in self._graphe_dict:
            print(f"Le sommet {sommet} n'existe pas")
        else:
            # On récupère tous les voisins et on construit une arête
            # partant du sommet et allant vers le voisin
            resultat = []
            for voisin in self._graphe_dict[sommet]:
                arete = {sommet, voisin}
                resultat.append(arete)

            return resultat

    def all_sommets(self):
        """
            Retourne la liste de tous les sommets
        """
        # Utiliser set() avec un paramètre permet de le convertir
        # vers un set. En l'occurence, on prend la liste des clés
        # et on la transforme en un set des clés
        return set(self._graphe_dict.keys())

    def all_aretes(self):
        """
            Retourne la liste de toutes les arrêtes du graphe
        """
        return self.__list_aretes()

    def add_sommet(self, sommet):
        """
            Ajoute un sommet s'il n'existe pas
        """
        # On vérifie si le sommet fait partie des clés
        if sommet not in self._graphe_dict:
            # Au départ, ce sommet n'a pas de voisins
            self._graphe_dict[sommet] = set()

    def add_arete(self, arete):
        """
            Ajoute une arete
        """
        # On extrait les deux sommets de l'arête passée en parametre
        som1, som2 = arete
        
        # Cette étape permet de s'assurer que les sommets existent
        # bien. Ils ne sont pas ajoutés s'ils existent déjà
        self.add_sommet(som1)
        self.add_sommet(som2)

        # som1 devient voisin de som2 et som2 devient voisin de som1
        self._graphe_dict[som1].add(som2)
        self._graphe_dict[som2].add(som1)

    def __list_aretes(self):
        # Le résultat est un set, ce qui fait que les arêtes ne seront
        # pas en double (par exemple, on évite {A,B} et {B,A} en même temps)
        resultat = set()

        # On parcourt tous les sommets du graphe, et pour chaque sommet,
        # on construit les arêtes qu'il partage avec ses voisins
        for sommet in self._graphe_dict.keys():
            for voisin in self._graphe_dict[sommet]:
                # On ne peut pas créer un set contenant des set. On est
                # obligé de passer par un frozenset. Un frozenset, à la 
                # différence du set, ne peut pas être modifié par la suite
                f = frozenset((sommet, voisin))
                resultat.add(f)

        return resultat
    
    def all_arcs(self):
        # Retourne la liste de tous les arcs sans doublon
        resultat = set()

        # On parcourt tous les sommets du graphe et on construit chaque arc
        for sommet in self._graphe_dict:
            for voisin in self._graphe_dict[sommet]:
                arc = (sommet, voisin)
                # Il n'y a normalement pas de risque de doublon, car AB et BA
                # sont deux arêtes différentes
                resultat.add(arc)

        return resultat

    # Les deux prochaines fonctions permettent d'utiliser le graphe
    # dans une boucle for (par exemple, for sommet in graphe)
    def __iter__(self):
        # Fonction propre à python
        self._iter_obj  = iter(self._graphe_dict)
        return self._iter_obj

    def __next__(self):
        # Fonction propre à python
        return next(self._iter_obj)

    def __str__(self):
        # Fonction propre à python permettant de convertir l'objet en chaine
        # de caractères. Cette fonction est appelée par print(), comme un 
        # toString en Java
        res = "sommets : "
        for k in self._graphe_dict:
            res += str(k) + " "

        res += "\naretes : "
        for arete in self.__list_aretes():
            som1, som2 = arete
            res += f"({som1}, {som2}) "
        res += "\n"
        return res

    def trouve_chaine(self, sommet_dep, sommet_arr, chaine=None):
        # Cherche une chaine élémentaire entre sommet_dep et sommet_arr

        if sommet_dep == sommet_arr:
            # la chaine est trouvé, on remonte la pile d'execution
            return chaine + [sommet_dep] if chaine is not None else [sommet_dep]
        else:
            # On relance la fonction avec le nouveau sommet
            for s in self._graphe_dict[sommet_dep]:
                new_chaine = chaine + [sommet_dep] if chaine is not None else [sommet_dep]
                if s not in new_chaine:
                    # on ne garde que les sommets qui n'ont jamais été trouvé
                    res = self.trouve_chaine(s, sommet_arr, new_chaine)
                    if res is not None:
                        return res

    def trouve_tous_chemins(self, sommet_dep, sommet_arr, chaine=None, chem=None):
        # Cherche l'ensemble des chemins

        if chem is None:
            chem = []

        if sommet_dep == sommet_arr:
            # On stoppe la propagation de la recursivité
            chem.append(chaine + [sommet_dep] if chaine is not None else [sommet_dep])
        else:
            # On relance la fonction avec le nouveau sommet
            for s in self._graphe_dict[sommet_dep]:
                new_chaine = chaine + [sommet_dep] if chaine is not None else [sommet_dep]
                if s not in new_chaine:
                    self.trouve_tous_chemins(s, sommet_arr, new_chaine, chem)

        return chem


class Graphe2 (Graphe):
    def sommet_degre(self, sommet):
        """ Degré d'un sommet """

        # On compte le nombre de voisins au sommet
        degre = len(self._graphe_dict[sommet])

        # Si le sommet est voisin avec lui-même, cela signifie 
        # qu'il y a une boucle : on compte deux fois l'arête
        if sommet in self._graphe_dict[sommet]:
            degre += 1

        return degre

    def trouve_sommet_isole(self):
        """ Renvoie la liste des sommets """
        # On parcourt chaque sommet, et on ne garde que ceux ayant
        # un set vide, c'est-à-dire aucun voisin
        resultat = []
        for sommet, voisins in self._graphe_dict.items():
            if voisins == set():
                resultat.append(sommet)

        return sommet

    def list_degres(self):
        # On calcule le degré pour chaque sommet
        degres = []
        for sommet in self._graphe_dict:
            degres.append(self.sommet_degre(sommet))
        
        # On trie dans l'ordre décroissant
        degres_trie = sorted(degres, reverse=True)

        # On retounre sous forme de tuple
        return tuple(degres_trie)
        
    def Delta(self):
        # Retourne le plus grand degré parmi tous les sommets
        return max(self.list_degres())


# Cette condition fait que les instructions suivantes ne sont exécutées
# que si c'est bien ce fichier que l'on appelle avec Python, et non
# pas un autre programme python qui l'appelle. Utile pour faire des tests
if __name__ == "__main__":
    graphe_data1 = {
        "A" : {"C"},
        "B" : {"C", "E"},
        "C" : {"A", "B", "D", "E"},
        "D" : {"C"},
        "E" : {"C", "B"},
        "F" : set()
    }

    graphe_data2 = {
        "A" : {"D", "F"},
        "B" : {"C"},
        "C" : {"B", "C", "D", "E"},
        "D" : {"A", "C", "F"},
        "E" : {"C"},
        "F" : {"A", "D"}
    }

    g1 = Graphe(graphe_data1)
    g2 = Graphe(graphe_data2)

    g_type2 = Graphe2(graphe_data1)
    
    print(g1.aretes("A"))
    
    print(g1.trouve_tous_chemins("A", "B"))
    print(g2.trouve_tous_chemins("A", "F"))
    print(g_type2.sommet_degre("C"))
    print(g_type2.trouve_sommet_isole())
    print(g_type2.Delta())
    print(g_type2.list_degres())
    print(g_type2.all_aretes())

    # vérification du lemme
    somme_degres = sum(g_type2.list_degres())
    nb_aretes = len(g_type2.all_aretes())

    if 2 * nb_aretes == somme_degres:
        print("Lemme des poignées de main vérifié")
    else:
        print("Lemme des poignées de main non vérifié")
