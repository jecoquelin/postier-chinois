# # # implémentation d'une file avec des fonctions
def file():
  #retourne une liste vide
  return []

# vide
def vide(f):
  """renvoie True si la file est vide  et False sinon"""
  return f==[]

# empiler
def enfiler(f,x):
  """Ajoute l’élément x à la file p"""
  f.append(x)
  
# dépiler
def defiler(f):
  """dépile et renvoie l’élément au sommet de la pile p"""
  assert not vide(f)
  return f.pop(0)

def taille(f):
  taille_liste=0
  # e: élement dans la liste p
  for e in f:
    taille_liste=taille_liste+1
  return taille_liste

def sommet(f):
  return f[0]



# # # Implémentation d'un file en POO
class File:
  """classe File 
  création d’une instance File avec une liste"""
  def __init__(self):
    """Initialisation d’une file vide"""
    self.L=[]

  def vide(self):
    """teste si la file est vide"""
    return self.L==[]

  def defiler(self):
    """dépile"""
    assert not self.vide(),"Pile vide"
    return self.L.pop(0)

  def enfiler(self,x):
    """enfile"""
    self.L.append(x)

  def taille(self):
    """retourne la taille de la file"""
    taille_file=0
    # e: élement dans la liste f
    for e in self.L:
      taille_file=taille_file+1
    return taille_file

  def sommet(self):
    """retourne le sommet de la file"""
    return self.L[0]
    

        