#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  3 08:13:25 2022

@author: jecoquelin
"""


class Graphe(object): 
    def __init__(self,graphe_dict=None):
        if graphe_dict==None:
            graphe_dict={}
        self._graphe_dict = graphe_dict
    
    def aretes(self,sommet):
        """retourne une liste de toutes les aretes d'un sommet"""
        return self._graphe_dict[sommet]
    
    def all_sommets(self):
        """retourne tous les sommets du graphe"""
        return self._graphe_dict.keys()
    
    def all_aretes(self):
        """retourne toutes les aretes du graphe à 
        partir de la méthode privée,_list_aretes,à définir plus bas.Ici on fera 
        donc simplement appel à cette méthode."""
        
        return self.__list_aretes()
    
    def add_sommet(self,sommet):
        """Si le sommet n'est pas déjà 
        présent dans le graphe, on rajoute 
        au dictionaire une clé sommet
        avec une liste vide pour valeur. Sinon on ne fait rien."""

        if sommet not in self._graphe_dict:
            self._graphe_dict[sommet]=set({})
        else :
            print("Sommet déjà présent")
        
    
    def add_arete(self, arete):
        sommet1,sommet2=tuple(arete)
        self.add_sommet(sommet1)
        self.add_sommet(sommet2)
        (self._graphe_dict[sommet1]).add(sommet2)
        (self._graphe_dict[sommet2]).add(sommet1)
    
    def __list_aretes(self) :
        """ Méthode privée pour récupérer les aretes.
        Une arete est un ensemble (set) avec un (boucle) ou
        deux sommets """
        liste = []
        for k in self : 
            liste.append(self._graphe_dict[k])
        return liste
    
    def __iter__(self) : 
        """ on crée un itérable à partir d'un graphe """
        self._iter_obj = iter(self._graphe_dict)
        return self._iter_obj
    
    def __next(self) : 
        """ Pour itérer sur les sommets du graphe """
        return next(self._iter_obj)
    
    def __str__(self) : 
        res = "sommets: "
        for k in self._graphe_dict :
            res += str(k) + " "
        res += "\naretes: "
        for arete in self.__list_aretes():
            res += str(arete) + " "
        return res 
    
    def trouve_chaine(self,sommet_dep,sommet_arr,chaine=None):
        """ Cherche une chaine élémentaire entre sommet_dep et sommet_arr"""

        if sommet_dep == sommet_arr:
           
            return chaine + [sommet_dep] if chaine is not None else [sommet_dep]

        else:
           
            new_chaine = chaine + [sommet_dep] if chaine is not None else [sommet_dep]
            for s in self._graphe_dict[sommet_dep]:
                if s not in new_chaine:
                 
                    res = self.trouve_chaine(s, sommet_arr, new_chaine)
                    if res is not None:
                        return res
        
    def trouve_tous_chemin(self,sommet_dep, sommet_arr,chaine = None, chem=[]):
        """ Méthode pour trouver tous les chemins de sommet_dep à sommet_art"""

        if chem is None:
            chem = []

        if sommet_dep == sommet_arr:
            # on stoppe la propagation de la recursivité
            chem.append(chaine + [sommet_dep] if chaine is not None else [sommet_dep])
        else:
            # On relance la fonction avec le nouveau sommet
            new_chaine = chaine + [sommet_dep] if chaine is not None else [sommet_dep]
            for s in self._graphe_dict[sommet_dep]:
                if s not in new_chaine:
                    self.trouve_tous_chemins(s, sommet_arr, new_chaine, chem)

        return chem
    
class Graphe2(Graphe):
    def get_sommet_degre(self,sommet):
        """ renvoie le degré du sommet"""
        degre = aretes(self,sommet)
        return len(degre)
        
    def trouve_sommet_isole(self):
        """ renvoie la liste des sommets isolés"""
        return isoles
        
    def Delta(self):
        """ Le degré maximum"""
        return max
    
    def list_degres(self):
        """ Calcule tous les degré et renvoir un tuple de degres decroissant"""
        
        return degres
        
    