# Ce TP introduit les graphes valués et les algorithmes de Dijkstra et Bellman-Ford
from TP1 import Graphe

class GrapheValue (Graphe):
    # On commence par appliquer les modifications propres aux graphes valués
    def add_arete(self, arete, valeur):
        """
        Ajoute une arete
        """
        # On extrait les deux sommets de l'arête passée en parametre
        som1, som2 = arete

        # Cette étape permet de s'assurer que les sommets existent
        # bien. Ils ne sont pas ajoutés s'ils existent déjà
        self.add_sommet(som1)
        self.add_sommet(som2)

        # som1 devient voisin de som2 et som2 devient voisin de som1
        self._graphe_dict[som1].add({som2: valeur})
        self._graphe_dict[som2].add({som1: valeur})


    def add_arc(self, arete, valeur):
        som1, som2 = arete

        # Cette étape permet de s'assurer que les sommets existent
        # bien. Ils ne sont pas ajoutés s'ils existent déjà
        self.add_sommet(som1)
        self.add_sommet(som2)

        # som1 devient voisin de som2 et som2 devient voisin de som1
        self._graphe_dict[som1].add({som2: valeur})

    
    def poids(self, arete):
        # Retourne le poids d'une arête
        som1, som2 = arete
        return self._graphe_dict[som1][som2]


    # ALGORITHMES
    def dijkstra(self, sommet_depart):
        # Dictionnaire des distances au sommet de départ, à l'infini
        distances = {som: float("inf") for som in self.all_sommets()}
        distances[sommet_depart] = 0

        # Liste des parents
        parents = dict()

        # Liste des sommets à visiter
        liste = list(self.all_sommets())

        while len(liste) > 0:
            # On recherche le sommet ayant la plus petite distance
            distance_min = None
            sommet_min = None
            for s in liste:
                # On vérifie que le sommet a une distance
                if s in distances:
                    if sommet_min is None or distances[s] < distance_min:
                        sommet_min = s
                        distance_min = distances[s]
            
            # On le retire des sommets à parcourir
            sommet_actuel = sommet_min
            liste.remove(sommet_actuel)

            # On récupère l'intersection entre les sommets à parcourir
            # et le voisins du sommets pour obtenir les prochains sommets
            prochains_sommets = set(self._graphe_dict[sommet_actuel]) & set(liste)
            for voisin in prochains_sommets:
                # Distance du sommet de départ, passant par le sommet actuel et terminant
                # à ce voisin
                distance_actuelle = distances[sommet_actuel] + self.poids((sommet_actuel, voisin))

                # Sinon, si une distance est calculée mais qu'elle est plus longue que celle
                # passant par ce chemin, on la remplace
                if distances[voisin] > distance_actuelle:
                    distances[voisin] = distance_actuelle

                    # On remplace le sommet parent comme étant le sommet actuel
                    parents[voisin] = sommet_actuel

        return (distances, parents)


    def plus_court_chemin(self, depart, arrivee):
        # Il suffit de récupérer le tableau des parents de dijkstra et de reconstruitre
        # le chemin
        _, parents = self.dijkstra(depart)

        sommet_actuel = arrivee
        chemin = []
        # Tant que le sommet a un parent, on l'ajoute
        while sommet_actuel in parents:
            chemin.insert(0, sommet_actuel)
            sommet_actuel = parents[sommet_actuel]

        # Enfin on ajoute le sommet de départ
        chemin.insert(0, depart)

        return chemin


    def bellman_ford(self, sommet_depart):
        # Effectue l'algorithme de bellman_ford à partir du sommet passé en
        # paramètres

        # Dictionnaire des distances par rapport au sommet de départ
        distances = {som: float("inf") for som in self.all_sommets()}
        distances[sommet_depart] = 0

        # Liste des parents
        parents = dict()

        # On répète l'action n-1 fois, n étant l'ordre du graphe
        for i in range(1, len(self.all_sommets())):
            # On récupère tous les arcs
            for u, v in self.all_arcs():
                distance_actuelle = distances[u] + self.poids((u, v))
                # Si la distance actuelle est plus petite que celle existant, on la remplace
                if distances[v] > distance_actuelle:
                    distances[v] = distance_actuelle
                    parents[v] = u

        # On vérifie l'efficacité de l'algorithme
        for u, v in self.all_arcs():
            if distances[v] > distances[u] + self.poids((u, v)):
                return False

        return distances, parents


    def bellman_ford_redondance(self, sommet_depart):
        # Effectue l'algorithme de bellman_ford à partir du sommet passé en
        # paramètres, tout en évitant les redondances

        # Dictionnaire des distances par rapport au sommet de départ
        distances = {som: float("inf") for som in self.all_sommets()}
        distances[sommet_depart] = 0

        # Liste des parents
        parents = dict()

        # On répète l'action n-1 fois, n étant l'ordre du graphe
        i = 1
        mise_a_jour = True
        while i < len(self.all_sommets()) and mise_a_jour:
            mise_a_jour = False
            # On récupère tous les arcs
            for u, v in self.all_arcs():
                distance_actuelle = distances[u] + self.poids((u, v))
                # Si la distance actuelle est plus petite que celle existant, on la remplace
                if distances[v] > distance_actuelle:
                    distances[v] = distance_actuelle
                    parents[v] = u
                    # Une modification a été apportée, on continue l'algo
                    mise_a_jour = True

        # On vérifie l'efficacité de l'algorithme
        for u, v in self.all_arcs():
            if distances[v] > distances[u] + self.poids((u, v)):
                return False

        return distances, parents


if __name__ == "__main__":
    graphe_data = {
        "A": {"C": 1},
        "B": {"C": 2, "E": 3},
        "C": {"A": 1, "B": 2, "D": 4, "E": 2},
        "D": {"C": 4},
        "E": {"C": 2, "B": 3}
    }

    graphe_data2 = {
        1: {2: 10, 3: 3},
        2: {3: 1, 4: 2},
        3: {2: 4, 4: 8, 5: 2},
        4: {5: 7},
        5: {4: 9}
    }

    g = GrapheValue(graphe_data2)
    print(g.all_arcs())
    print(g.all_sommets())
    print(g.poids((1, 2)))
    print(g.dijkstra(1))
    print(g.bellman_ford(1))
    print(g.bellman_ford_redondance(1))
    print(g.plus_court_chemin(1, 4))
