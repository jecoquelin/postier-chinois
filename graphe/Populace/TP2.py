# Ce TP introduit les degrés et les chemins
from TP1 import Graphe

class GrapheDegre (Graphe):
    def sommet_degre(self, sommet):
        """ Degré d'un sommet """

        # On compte le nombre de voisins au sommet
        degre = len(self._graphe_dict[sommet])

        # Si le sommet est voisin avec lui-même, cela signifie 
        # qu'il y a une boucle : on compte deux fois l'arête
        if sommet in self._graphe_dict[sommet]:
            degre += 1

        return degre

    def trouve_sommet_isole(self):
        """ Renvoie la liste des sommets """
        # On parcourt chaque sommet, et on ne garde que ceux ayant
        # un set vide, c'est-à-dire aucun voisin
        resultat = []
        for sommet, voisins in self._graphe_dict.items():
            if voisins == set():
                resultat.append(sommet)

        return sommet

    def list_degres(self):
        # On calcule le degré pour chaque sommet
        degres = []
        for sommet in self._graphe_dict:
            degres.append(self.sommet_degre(sommet))
        
        # On trie dans l'ordre décroissant
        degres_trie = sorted(degres, reverse=True)

        # On retounre sous forme de tuple
        return tuple(degres_trie)
        
    def Delta(self):
        # Retourne le plus grand degré parmi tous les sommets
        return max(self.list_degres())


class GrapheChemin (Graphe):
    def trouve_chaine(self, sommet_dep, sommet_arr, chaine=None):
        # Cherche une chaine élémentaire entre sommet_dep et sommet_arr

        if sommet_dep == sommet_arr:
            # la chaine est trouvé, on remonte la pile d'execution
            return chaine + [sommet_dep] if chaine is not None else [sommet_dep]
        else:
            # On relance la fonction avec le nouveau sommet
            for s in self._graphe_dict[sommet_dep]:
                new_chaine = chaine + [sommet_dep] if chaine is not None else [sommet_dep]
                if s not in new_chaine:
                    # on ne garde que les sommets qui n'ont jamais été trouvé
                    res = self.trouve_chaine(s, sommet_arr, new_chaine)
                    if res is not None:
                        return res


    def trouve_tous_chemins(self, sommet_dep, sommet_arr, chaine=None, chem=None):
        # Cherche l'ensemble des chemins

        if chem is None:
            chem = []

        if sommet_dep == sommet_arr:
            # On stoppe la propagation de la recursivité
            chem.append(chaine + [sommet_dep] if chaine is not None else [sommet_dep])
        else:
            # On relance la fonction avec le nouveau sommet
            for s in self._graphe_dict[sommet_dep]:
                new_chaine = chaine + [sommet_dep] if chaine is not None else [sommet_dep]
                if s not in new_chaine:
                    self.trouve_tous_chemins(s, sommet_arr, new_chaine, chem)

        return chem