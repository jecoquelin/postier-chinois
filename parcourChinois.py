#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 16:08:42 2022

@author: khamard
"""

# Retourne la liste des sommets du graphe
def all_sommet(graphe):
    return set(graphe)

# Ajoute au graphe une nouvelle aretes entre deux sommets avec une pondération de 1
def add_arete(graphe, arete):
    som1, som2 = arete

    if (som1 or som2) not in all_sommet(graphe):
        print("L'un des deux sommet n'existe pas")
        return 0

    (graphe[som1]).update({som2 : 1})
    (graphe[som2]).update({som1 : 1})

# retourne une liste des sommets impaire du graphe
def sommet_impaire(graphe):
    liste = []
    for sommet in graphe:
        if len(graphe[sommet]) % 2 == 1 :
            liste.append(sommet)
    return liste


# Fait le produit cartesien entre les differents sommets de la liste (ici des sommets impaires)
def produit_cartesien(liste) :

    liste_aretes = []
    for i in range (0, len(liste)) :
        for j in range (0, len(liste)) :
            if (not (i == j) ) :
                liste_aretes.append( (liste[i], liste[j]) )

    return liste_aretes


# Fonction retournant la liste des aretes existantes
def aretes_impaires(graphe, liste):

    listeAretesUtilisable = []

    for i in liste :
        for voisin in graphe[i].keys() :
            if voisin in liste :
                listeAretesUtilisable.append((i, voisin))

    return listeAretesUtilisable


# Fonction retournant la liste des aretes utilisable pour rendre le graphe pair
def aretes_utilisable(toutes_aretes, aretes_existantes) :

    setL1 = set(toutes_aretes)

    setL2 = set(aretes_existantes)

    liste_aretes_utilisable = ( setL1 - setL2 )

    liste_aretes_utilisable = list(liste_aretes_utilisable)

    return liste_aretes_utilisable


# Fonction rendant le graphe avec que des sommets de degré pair, le graphe devient eulérien à la fin de cette procédure
def rendre_tous_pair(graphe, liste, liste_arete):

    while len(liste) > 0 :
        i=0
        trouve = False
        sommet = liste[0]
        while ( (not trouve) and (i < len(liste_arete)) ):
            x, y = liste_arete[i]
            if sommet in [x, y]:
                add_arete(graphe, [x, y])
                liste.remove(y)
                liste.remove(x)
                j = 0
                while (j < len(liste_arete)):
                    a, b = liste_arete[j]
                    if len(set([a,b]) & set([x,y])):
                        liste_arete.remove((a, b))
                        j-= 1
                    j+= 1
                trouve = True
            i+= 1


# fonction principale qui retourne le poid du cycle passant par toutes les aretes
def parcourChinois(graphe, sommet_dep):

    print(graphe)

    listeSommetImpaire = sommet_impaire(graphe) # Donne la liste des sommets impaires du graphe donné
    liste_toutes_aretes = produit_cartesien(listeSommetImpaire) # Donne la liste du produit cartésien entre tous les sommets impaires
    liste_aretes_existante = aretes_impaires(graphe, listeSommetImpaire) # Donne la liste des aretes du graphe donné qui existe déjà

    liste_aretes_utilisable = aretes_utilisable(liste_toutes_aretes, liste_aretes_existante) # Donne la liste des arretes utilisables pour rendre le graphe eulérien

    rendre_tous_pair(graphe, listeSommetImpaire, liste_aretes_utilisable) # Rend le graphe eullérien

    print(graphe)

    # Trouver le circuit eulérien du graphe et sa pondération total.



    return 0



graphe = { "A" : {"B" : 10, "C" : 3},
           "B" : {"A" : 10, "F" : 5},
           "C" : {"D" : 3, "E" : 2, "A" : 3},
           "D" : {"C" : 3},
           "E" : {"C" : 2},
           "F" : {"B" : 5}
        }


#print(graphe["A"])
#print(g1.all_sommet())
#parcourChinois(graphe, "A")
#print(sommet_impaire(graphe))
parcourChinois(graphe, "A")
#print(set(graphe))