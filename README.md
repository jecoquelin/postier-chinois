# Postier Chinois

Ce dépôt contient le problème du postier chinois.

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Cette SAE nous a fait découvrir l'environnement des graphes qui sont très importants pour faire une table de routage en réseau par exemple.

Cette SAE s'est déroulée en 3 étapes :
1. Parmi 3 problèmes qui sont le problème du cavalier, les huit dames ou encore le postier chinois ; nous devions en choisir un.
2. Nous avons mis en œuvre une solution pour arriver à bout de ce problème avec un script réalisé sous python3. Nous avons enfin fait une présentation orale de notre travail.

## Apprentissage critique

AC12.01. Analyser un problème avec méthode<br>
AC12.02. Comparer des algorithmes pour des problèmes classiques<br>
AC12.03. Formaliser et mettre en œuvre des outils mathématiques pour l’informatique

## Langage et Framework 
  * Python

## Prérequis 
Afin de pouvoir exécuter l'application sur votre poste, vous devrez avoir :
  * python

## Exécution
Pour lancer le problème, il vous suffit de télécharger le site web et de lancer le fichier `pyton postier-chinois`. 