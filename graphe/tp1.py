#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 08:11:52 2022

@author: jecoquelin
"""

class Graphe(object): 
    def __init__(self,graphe_dict=None):
        if graphe_dict==None:
            graphe_dict={}
        self._graphe_dict = graphe_dict
    
    def aretes(self,sommet):
        """retourne une liste de toutes les aretes d'un sommet"""
        return self._graphe_dict[sommet]
    
    def all_sommets(self):
        """retourne tous les sommets du graphe"""
        return self._graphe_dict.keys()
    
    def all_aretes(self):
        """retourne toutes les aretes du graphe à 
        partir de la méthode privée,_list_aretes,à définir plus bas.Ici on fera 
        donc simplement appel à cette méthode."""
        
        return self.__list_aretes()
    
    def add_sommet(self,sommet):
        """Si le sommet n'est pas déjà 
        présent dans le graphe, on rajoute 
        au dictionaire une clé sommet
        avec une liste vide pour valeur. Sinon on ne fait rien."""

        if sommet not in self._graphe_dict:
            self._graphe_dict[sommet]=set({})
        else :
            print("Sommet déjà présent")
        
    
    def add_arete(self, arete):
        sommet1,sommet2=tuple(arete)
        self.add_sommet(sommet1)
        self.add_sommet(sommet2)
        (self._graphe_dict[sommet1]).add(sommet2)
        (self._graphe_dict[sommet2]).add(sommet1)
    
        def __list_aretes(self) :
        """ Méthode privée pour récupérer les aretes.
        Une arete est un ensemble (set) avec un (boucle) ou
        deux sommets """
        liste = []
        for k in self : 
            liste.append(self._graphe_dict[k])
        return liste
    
    def __iter__(self) : 
        """ on crée un itérable à partir d'un graphe """
        self._iter_obj = iter(self._graphe_dict)
        return self._iter_obj
    
    def __next(self) : 
        """ Pour itérer sur les sommets du graphe """
        return next(self._iter_obj)
    
    def __str__(self) : 
        res = "sommets: "
        for k in self._graphe_dict :
            res += str(k) + " "
        res += "\naretes: "
        for arete in self.__list_aretes():
            res += str(arete) + " "
        return res 