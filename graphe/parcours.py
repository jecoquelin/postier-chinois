from graphe import Graphe
#import numpy as np

# GrapheParcours reprend toutes les méthodes et tous les attributs de Graphe
# (all_aretes, all_sommets...)
class GrapheParcours(Graphe):
    
    def add_arete(self, arete, valeur):
        """
        Ajoute une arete
        """
        # On extrait les deux sommets de l'arÃªte passÃ©e en parametre
        som1, som2 = arete

        # Cette Ã©tape permet de s'assurer que les sommets existent
        # bien. Ils ne sont pas ajoutÃ©s s'ils existent dÃ©jÃ 
        self.add_sommet(som1)
        self.add_sommet(som2)

        # som1 devient voisin de som2 et som2 devient voisin de som1
        self._graphe_dict[som1].add({som2: valeur})
        self._graphe_dict[som2].add({som1: valeur})


    def add_arc(self, arete, valeur):
        som1, som2 = arete

        # Cette Ã©tape permet de s'assurer que les sommets existent
        # bien. Ils ne sont pas ajoutÃ©s s'ils existent dÃ©jÃ 
        self.add_sommet(som1)
        self.add_sommet(som2)

        # som1 devient voisin de som2 et som2 devient voisin de som1
        self._graphe_dict[som1].add({som2: valeur})

    
    def poids(self, arete):
        # Retourne le poids d'une arÃªte
        som1, som2 = arete
        return self._graphe_dict[som1][som2]
    
    
    

    def parcours_largeur(self, sommet_depart):
        # La file contiendra les sommets qu'on aura visité
        # Une file est de type FIFO
        file = [sommet_depart]
        
        # Le dictionnaire contiendra les distance avec le point de départ
        distances = dict()

        # Le sommet de départ aura une distance de 0 par rapport a lui-même
        distances[sommet_depart] = 0


        graphe_parcours = Graphe()

        # Début du parcours
        while len(file) > 0:
            # On parcourt le graphe tant que la file n'est pas vide et 
            # donc tant qu'il y aura un sommet à parcourir
             
            sommet_actuel = file.pop(0)     # On enlève le premier élément (FIFO)

            # On regarde les voisins du sommet actuel
            for voisin in self._graphe_dict[sommet_actuel]:
                
                # Si une distance est calculé alors le sommet est déjà visité et 
                # parcouru sinon il n'as pas été visité et donc pas parcouru
                
                if voisin not in distances:
                    # On calule la distance du sommet par rapport au parent
                    distances[voisin] = distances[sommet_actuel] + 1
                    # On l'ajoute aux prochains sommets à parcourir (LIFO)
                    file.append(voisin)
                    # On ajoute l'arête dans le graphe
                    graphe_parcours.add_arete({sommet_actuel, voisin})

        return (distances, graphe_parcours)

    def parcours_profondeur(self, sommet_depart):
        # Le parcours en profondeur reprend le même principe 
        # qu'un parcours en largeur mais avec une pile qui 
        # est de type LIFO (dernier entré, premier sorti)

        # La pile contiendra les sommet à parcourir donc nous 
        # mettons le sommet de départ pour commencer
        pile = [sommet_depart]

        # Graphe qu'on parcour
        graphe_parcours = Graphe()

        # La distance est un dictionnaire
        distances = dict()

        # La distance du sommet de départ par rapport à lui même est 0 ce 
        # qui est logique
        distances[sommet_depart] = 0

        # Début du parcours
        while len(pile) > 0:
            sommet_actuel = pile.pop()     # On enlève le dernier élément (LIFO)

            # Regarde les voisins de la variable "sommet_actuel"

            for voisin in self._graphe_dict[sommet_actuel]:

                # Si une distance est calculé alors le sommet est déjà visité et 
                # parcouru sinon il n'as pas été visité et donc pas parcouru

                if voisin not in distances:

                    # Nous calculons le degré du voisin en prenant le degré du voisin_actuel + 1
                    distances[voisin] = distances[sommet_actuel] + 1
                    pile.append(voisin)                                 # Nous ajoutons ce voisin à la pile
                    graphe_parcours.add_arete({sommet_actuel, voisin})  # Nous ajoutons une arrete au graphe entre le sommet actuel et le voisin 

        return (distances, graphe_parcours)
    
    def chemin_existe(self, sommet_depart, sommet_arrive):
        # Utilise le parcours en profondeur pour vérifier si un chemin
        # existe
        file = [sommet_depart]
        resultat = sommet_depart == sommet_arrive

        # On garde les sommets parcourus en mémoire
        parcourus = [sommet_depart]

        while len(file) > 0 and not resultat:
            # On récupère le sommet à parcourir
            sommet_actuel = file.pop(0)
            for sommet_voisin in self._graphe_dict[sommet_actuel]:
                # Si le sommet voisin 
                if sommet_voisin == sommet_arrive:
                    resultat = True

                if sommet_voisin not in parcourus:
                    parcourus.append(sommet_voisin)
                    file.append(sommet_voisin)

        return resultat

    def connexe(self, sommet_depart):
        # Même concept que le parcours en largeur avec l'utilisation
        # d'une pile (structure LIFO : dernier entré premier sorti)

        # La pile des prochains sommets à parcourir
        pile = [sommet_depart]

        graphe_parcours = Graphe()
        distances = dict()

        distances[sommet_depart] = 0

        # Début du parcours

        while len(pile) > 0:
            sommet_actuel = pile.pop()     # On enlève le premier élément (LIFO)
            for voisin in self._graphe_dict[sommet_actuel]:
                if voisin not in distances:
                    distances[voisin] = distances[sommet_actuel] + 1
                    pile.append(voisin)
                    graphe_parcours.add_arete({sommet_actuel, voisin})
                    
        if (len(self.all_sommets()) == len(graphe_parcours.all_sommets())):
            return print("Le graphe est connexe")
        else :
            return print("Le graphe est pas connexe")


    def cycle(self, sommet_depart):
        # Même concept que le parcours en largeur avec l'utilisation
        # d'une pile (structure LIFO : dernier entré premier sorti)

        # La pile des prochains sommets à parcourir
        pile = [sommet_depart]

        graphe_parcours = Graphe()
        distances = dict()

        distances[sommet_depart] = 0

        # Début du parcours

        while len(pile) > 0:
            sommet_actuel = pile.pop()     # On enlève le premier élément (LIFO)
            for voisin in self._graphe_dict[sommet_actuel]:
                if voisin in distances :
                    return print("Il y a un cycle dans le graphe")
                if voisin not in distances:
                    distances[voisin] = distances[sommet_actuel] + 1
                    pile.append(voisin)
                    graphe_parcours.add_arete({sommet_actuel, voisin})
        return print("Il n'y a pas de cycle dans le graphe")

    def dijkstra(self, sommet_depart):
        # Nous mettons les distances du sommet de depart à la valeur infini avec float("inf")
        distances = { som: float("inf") for som in self.all_sommets() }

        # La distance du sommet de départ par rapport à lui même est 0 ce 
        # qui est logique
        distances[sommet_depart] = 0

        # dictionnaire de parents
        parents = dict()

        # Liste des sommets à visiter
        liste = list(self.all_sommets())

        
        while len(liste) > 0:
            # On recherche le sommet ayant la plus petite distance
            distance_min = None
            sommet_min = None
            for s in liste:
                # On vérifie que le sommet a une distance
                if s in distances:
                    if sommet_min is None or distances[s] < distance_min:
                        sommet_min = s
                        distance_min = distances[s]
            
            # On le retire des sommets à parcourir
            sommet_actuel = sommet_min
            liste.remove(sommet_actuel)

            # On récupère l'intersection entre les sommets à parcourir
            # et le voisins du sommets pour obtenir les prochains sommets
            prochains_sommets = set(self._graphe_dict[sommet_actuel]) & set(liste)
            for voisin in prochains_sommets:
                # Distance du sommet de départ, passant par le sommet actuel et terminant
                # à ce voisin
                distance_actuelle = distances[sommet_actuel] + self.poids((sommet_actuel, voisin))

                # Sinon, si une distance est calculée mais qu'elle est plus longue que celle
                # passant par ce chemin, on la remplace
                if distances[voisin] > distance_actuelle:
                    distances[voisin] = distance_actuelle

                    # On remplace le sommet parent comme étant le sommet actuel
                    parents[voisin] = sommet_actuel

        return (distances, parents)


    def bellman_ford(self, sommet_depart):
        # Effectue l'agorithme de bellman_ford avec le sommet_depart

        # Nous mettons les distances du sommet de depart à la valeur infini avec float("inf")
        distances = {som: float("inf") for som in self.all_sommets()}

        #La distance du sommet de départ par rapport à lui même est 0
        distances[sommet_depart] = 0

        # Liste des parents
        parents = dict()

        # On répète l'action n-1 fois, n étant l'ordre du graphe
        for i in range(1, len(self.all_sommets())):
            # On récupère tous les arcs
            for u, v in self.all_arcs():
                distance_actuelle = distances[u] + self.poids((u, v))
                # Si la distance actuelle est plus petite que celle existant, on la remplace
                if distances[v] > distance_actuelle:
                    distances[v] = distance_actuelle    #On remplace la distance de vu par la distance_actuelle
                    parents[v] = u

        # On vérifie l'efficacité de l'algorithme
        for u, v in self.all_arcs():
            if distances[v] > distances[u] + self.poids((u, v)):
                return False

        return distances, parents


if __name__ == "__main__":
    graphe_value = { 
        "A" : {"C" : 1},
        "B" : {"C" : 2, "E" : 3},
        "C" : {"A" : 1, "B" : 2, "D" : 4, "E" : 2},
        "D" : {"C" : 4},
        "E" : {"C" : 2, "B" : 3}
    } 
    
    graphe_data = {
        "A" : {"C"},
        "B" : {"C", "E"},
        "C" : {"A", "B", "D", "E"},
        "D" : {"C"},
        "E" : {"C", "B"},
        "F" : set()
    }

    graphe_data2 = {
        "A" : {"D", "F"},
        "B" : {"C"},
        "C" : {"B", "C", "D", "E"},
        "D" : {"A", "C", "F"},
        "E" : {"C"},
        "F" : {"A", "D"}
    }

    graphe_data3 = {
        "1" : {"2", "5"},
        "2" : {"1", "3", "5", "7"},
        "3" : {"2", "4", "5"},
        "4" : {"3", "6"},
        "5" : {"1", "2", "3", "6"},
        "6" : {"5", "7"},
        "7" : {"2", "6"}
    }
    
    graphe_value2 = { "A" : {"C" : -1},
                 "B" : {"C" : 2, "E" : 3},
                 "C" : {"A" : 1, "B" : -2, "D" : 4, "E" : 2},
                 "D" : {"C" : -4},
                 "E" : {"C" : 2, "B" : 3},
                 "F" : {}
                } 
    
    g = GrapheParcours(graphe_value)
    g1 = GrapheParcours(graphe_data)
    g2 = GrapheParcours(graphe_data2)
    g3 = GrapheParcours(graphe_data3)
    g4 = GrapheParcours(graphe_value2)

    # On récupère séparement pour chaque parcours et pour chaque
    # graphe le dictionnaire des distances et le graphe de parcours
    # généré
    
    d1_largeur, t1_largeur = g1.parcours_largeur("A")
    d2_largeur, t2_largeur = g2.parcours_largeur("A")
    d3_largeur, t3_largeur = g3.parcours_largeur("1")
#
    #d1_profondeur, t1_profondeur = g1.parcours_profondeur("A")
    #d2_profondeur, t2_profondeur = g2.parcours_profondeur("A")
    #d3_profondeur, t3_profondeur = g3.parcours_profondeur("1")

    # Affichage
    print("Parcours Largeur\n---------")
    print(d1_largeur, "\n", t1_largeur)
    print(d3_largeur, "\n", t3_largeur)
    print(d2_largeur, "\n", t2_largeur)

#    print("\nParcours Profondeur\n---------")
#    print(d1_profondeur, "\n", t1_profondeur)
#    print(d2_profondeur, "\n", t2_profondeur)
#    print(d3_profondeur, "\n", t3_profondeur)
#    
#    print("\nChemin\n---------")
#    print(g1.chemins("A","F"))
#    print(g2.chemins("A","F"))
#    
#    print("\nConnexe\n---------")
#    print(g1.connexe("A"))
#    print(g2.connexe("A"))
#    
#    print("\nCycle\n---------")
#    print(g1.cycle("A"))
#    print(g2.cycle("A"))
    print("\nDijkstra\n---------")
    print(g.dijkstra("A"))
#    print(g2.dijkstra("A"))
#    print("\nBellman ford\n---------")
#    print(g4.bellman_ford("A"))
    
    


