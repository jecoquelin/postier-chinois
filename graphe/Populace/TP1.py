# Le TP1 introduit les graphes

class Graphe:
    def __init__(self, graphe_dict=None):
        # Initialise un graphe. Sans dictionnaire passé (None), on utilise un
        # dictionnaire vide par défaut

        if graphe_dict is None:
            graphe_dict = {}

        # Le dictionnaire aura pour clés les sommets et pour valeur la liste
        # des sommets voisins
        self._graphe_dict = graphe_dict


    def add_sommet(self, sommet):
        # Ajoute un sommet s'il n'existe pas
        if sommet not in self._graphe_dict:
            self._graphe_dict[sommet] = set()


    def all_sommets(self):
        # Retourne l'ensemble des sommets du graphe
        # NB : seules les clés sont gardées dans la conversion en list
        return list(self._graphe_dict)


    # Les méthodes suivantes sont propres aux graphes non orientés
    # Dans un graphe non-orienté, une arête est représentée par un set

    def aretes(self, sommet):
        # Retourne les arêtes ayant pour extrémité le sommet passé en
        # paramètres

        # On vérifie si le sommet existe
        if sommet not in self._graphe_dict:
            print(f"Le sommet {sommet} n'existe pas")
        else:
            # On récupère tous les voisins et on construit l'arête
            resultat = []

            for voisin in self._graphe_dict[sommet]:
                arete = {sommet, voisin}
                resultat.append(arete)

            # Autre possibilité, en une seule ligne :
            # resultat = [{sommet, v} for v in self.graphe_dict[sommet]]

            return resultat

    
    def all_aretes(self):
        # Retourne la liste de toutes les arêtes sans doublon
        resultat = set()

        # On parcourt tous les sommets du graphe et on construit chaque arête
        for sommet in self._graphe_dict:
            for voisin in self._graphe_dict[sommet]:
                # On utilise un frozenset car il est impossible de mettre un
                # set dans un set en python
                arete = frozenset({sommet, voisin})
                # Comme le résultat est un set, tout doublon est supprimé,
                # donc si AB y est déjà, BA ne sera pas ajouté
                resultat.add(arete)

        return resultat


    def add_arete(self, arete):
        # Ajoute un arc au graphe. Si les sommets n'existent pas, ils sont créés

        # On casse l'arc pour obtenir les deux sommets le composant
        som1, som2 = arete

        self.add_sommet(som1)
        self.add_sommet(som2)

        # som1 a pour voisin som2, et inversement
        self._graphe_dict[som1].add(som2)
        self._graphe_dict[som2].add(som1)
        
    
    # Les méthodes suivantes sont propres aux graphes orientés
    # Dans un graphe orienté, un arc est représenté par un tuple
    # Pas demandé explicitement dans le TP, mais dans les TP d'après
    # on travaille avec les deux types de graphe

    def arcs(self, sommet):
        # Retourne les arcs ayant pour départ le sommet passé en paramètres

        # On vérifie si le sommet existe
        if sommet not in self._graphe_dict:
            print(f"Le sommet {sommet} n'existe pas")
        else:
            # On récupère tous les voisins et on construit l'arc
            resultat = []

            for voisin in self._graphe_dict[sommet]:
                arc = (sommet, voisin)
                resultat.append(arc)

            # Autre possibilité, en une seule ligne :
            # [(sommet, v) for v in self._graphe_dict[sommet]]

            return resultat


    def all_arcs(self):
        # Retourne la liste de tous les arcs sans doublon
        resultat = set()

        # On parcourt tous les sommets du graphe et on construit chaque arc
        for sommet in self._graphe_dict:
            for voisin in self._graphe_dict[sommet]:
                arc = (sommet, voisin)
                # Il n'y a normalement pas de risque de doublon, car AB et BA
                # sont deux arêtes différentes
                resultat.add(arc)

        return resultat


    def add_arc(self, arc):
        # Ajoute un arc au graphe. Si les sommets n'existent pas, ils sont créés

        # On casse l'arc pour obtenir les deux sommets le composant
        depart, arrivee = arc

        self.add_sommet(depart)
        self.add_sommet(arrivee)

        # Le départ a pour successeur l'arrivée
        self._graphe_dict[depart].add(arrivee)


    def __str__(self):
        # Fonction propre à python permettant de convertir l'objet en chaine
        # de caractères. Cette fonction est appelée par print(), comme un 
        # toString en Java
        res = "sommets : "
        for k in self._graphe_dict:
            res += str(k) + " "

        res += "\naretes : "
        for arete in self.all_aretes():
            som1, som2 = arete
            res += f"({som1}, {som2}) "

        return res
    

# Cette condition fait que son contenu n'est pas exécuté s'il est importé par
# un autre fichier (ex: import TP1). On ne le voit que si c'est ce fichier 
# qui est précisément exécuté
if __name__ == "__main__":
    # On définit un graphe orienté et un graphe non orienté
    data_non_or = {
        "A" : {"C"},
        "B" : {"C", "E"},
        "C" : {"A", "B", "D", "E"},
        "D" : {"C"},
        "E" : {"C", "B"},
        "F" : set()
    }

    # Le graphe fonctionne aussi avec des chiffres
    data_or = {
        1: {2},
        2: {5},
        3: {2, 5},
        4: {3, 6},
        5: {1},
        6: {5, 7},
        7: {2}
    }

    graphe_non_or = Graphe(data_non_or)
    graphe_or = Graphe(data_or)

    # On teste les méthodes d'affichage
    print(graphe_non_or.all_sommets())
    print(graphe_non_or.all_aretes())
    print(graphe_non_or.aretes("C"))

    print(graphe_or.all_sommets())
    print(graphe_or.all_arcs())
    print(graphe_or.arcs(6))

    # On teste les méthodes d'ajout
    graphe_non_or.add_sommet("G")
    graphe_non_or.add_sommet("A")   # Déjà existant
    graphe_non_or.add_arete({"A", "G"})

    graphe_or.add_sommet(8)
    graphe_or.add_sommet(1)         # Déjà existant
    graphe_or.add_arc((3, 8))

    print(graphe_non_or.all_aretes())
    print(graphe_or.all_arcs())