from graphe import Graphe

class Connexe(Graphe):

    def parcours_largeur(self, sommet_depart):
        # La file des prochains sommets à parcourir
        # Une file est de type FIFO : premier entré, premier sorti
        file = [sommet_depart]
        
        # Les distances de chaque sommet par rapport au sommet d'origine
        # Chaque clé correspond au sommet, et à chaque clé est associée un chiffre
        # représentant la distance
        distances = dict()

        # Évidemment, le sommet de départ a une distance de 0 par rapport à lui-même
        distances[sommet_depart] = 1
 
        # L'arbre de parcours représente les arêtes entre les sommets
        # parcourus. Il respecte les distances découvertes au fur et à
        # mesure
        graphe_parcours = Graphe()
        
        compteur = 1
        
        # Début du parcours
        while len(file) > 0:
            # On parcourt tant qu'il reste des sommets à parcourir,
            # autrement dit quand il y a toujours des éléments dans la file
            # donc sa taille est plus grande que 0

            sommet_actuel = file.pop(0)     # On enlève le premier élément (FIFO)

            # On récupère tous les voisins du sommet actuel
            for voisin in self._graphe_dict[sommet_actuel]:
                # On part du principe que si une distance a déjà été calculée
                # pour le voisin actuel, alors il a déjà été parcouru. On ne
                # va voir que les autres
                if voisin not in distances:
                    # On calcule la distance pour ce sommet voisin
                    distances[voisin] = 1
                    
                    # On ajoute un au compeur car un nouveau sommet est parcouru
                    compteur = compteur + 1
                    
                    # On l'ajoute aux prochains sommets à parcourir (LIFO)
                    file.append(voisin)
                    # On ajoute l'arête dans le graphe
                    graphe_parcours.add_arete({sommet_actuel, voisin})
        if compteur == len(self.all_aretes(self)):
            return True
        
        return False
    
    def chemins(self, sommet_depart, sommet_arrive):
        # Même concept que le parcours en largeur avec l'utilisation
        # d'une pile (structure LIFO : dernier entré premier sorti)
        
        # La pile des prochains sommets à parcourir
        pile = [sommet_depart]
        
        graphe_parcours = Graphe()
        distances = dict()

        distances[sommet_depart] = 0

        # Début du parcours
        
        #while pile[sommet_depart] != pile[sommet_arrive]
        while len(pile) > 0:
            sommet_actuel = pile.pop()     # On enlève le premier élément (LIFO)
            for voisin in self._graphe_dict[sommet_actuel]:
                if voisin not in distances:
                    distances[voisin] = distances[sommet_actuel] + 1
                    pile.append(voisin)
                    graphe_parcours.add_arete({sommet_actuel, voisin})

        return (distances)
    
    def cycle(self, sommet_depart):
        # Même concept que le parcours en largeur avec l'utilisation
        # d'une pile (structure LIFO : dernier entré premier sorti)
        
        # La pile des prochains sommets à parcourir
        pile = [sommet_depart]
        
        graphe_parcours = Graphe()
        distances = dict()

        distances[sommet_depart] = 0

        # Début du parcours
        
        while len(pile) > 0:
            sommet_actuel = pile.pop()     # On enlève le premier élément (LIFO)
            for voisin in self._graphe_dict[sommet_actuel]:
                if voisin is in distances :
                    return True
                if voisin not in distances:
                    distances[voisin] = distances[sommet_actuel] + 1
                    pile.append(voisin)
                    graphe_parcours.add_arete({sommet_actuel, voisin})
        return False
        
    
    if __name__ == "__main__":
    graphe_data = {
        "A" : {"C"},
        "B" : {"C", "E"},
        "C" : {"A", "B", "D", "E"},
        "D" : {"C"},
        "E" : {"C", "B"},
        "F" : set()
    }

    graphe_data2 = {
        "A" : {"D", "F"},
        "B" : {"C"},
        "C" : {"B", "C", "D", "E"},
        "D" : {"A", "C", "F"},
        "E" : {"C"},
        "F" : {"A", "D"}
    }

    graphe_data3 = {
        "1" : {"2", "5"},
        "2" : {"1", "3", "5", "7"},
        "3" : {"2", "4", "5"},
        "4" : {"3", "6"},
        "5" : {"1", "2", "3", "6"},
        "6" : {"5", "7"},
        "7" : {"2", "6"}
    }

    g1 = GrapheParcours(graphe_data)
    g2 = GrapheParcours(graphe_data2)
    g3 = GrapheParcours(graphe_data3)
    
    
    
    print(g1.chemin("A","F"))
    
    print(g1.)
    
    
    
    """"
    def dijkstra(self, sommet_depart):
        
        file = [sommet_depart]
        distances = dict()
        distances[sommet_depart] = 0"""